import SwipeRender from 'react-native-swipe-render';
import React, { useState, useEffect } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity, LayoutAnimation, Platform, UIManager, Button, TextInput, Image,
} from 'react-native';
import Constants from 'expo-constants';
import { Overlay, Input } from 'react-native-elements';
import * as SQLite from 'expo-sqlite';


if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

/**************************************************
 * Function:    openDatabase
 * 
 * Description: Opens SQLite DB
 * 
 * Props:       none
 **************************************************/
function openDatabase() {
  const db = SQLite.openDatabase('db.db');
  return db;
}

const db = openDatabase();

/**************************************************
 * Component:   App
 * 
 * Description: Main function component for app
 * 
 * Props:       none
 **************************************************/
export default function App() {
  useEffect(() => {
    db.transaction((tx) => {
      tx.executeSql(`create table if not exists abilities (id integer primary key not null, title text, desc text, uses int);`);
    });
  }, []);

  return (
    <SwipeRender
      index={0}
      showsPagination={true}
    >
      {/* Page 1 */}
      <View>
        <StatPage/>
      </View>

      {/* Page 2 */}
      <View style={styles.abilPage}>
        <AbilityDrop/>
      </View>

      {/* Page 3 */}
      <View style={styles.container}>
        <Dropdown title='Page 3' data='Test1'/>
        <Text></Text>
      </View>
    </SwipeRender>
  );
}

/**************************************************
 * Component:   Stat Page
 * 
 * Description: Stat page component containing and
 *              dealing with stat updates
 * 
 * Props:       none
 **************************************************/
function StatPage(props) {
  const editStat = () => {

  }

  return (
    <View style={styles.statPage}>
      <StatBox stat='Strength' val={10} prof={0}/>
      <StatBox stat='Intelligence' val={10} prof={0}/>
      <StatBox stat='Dexterity' val={10} prof={0}/>
      <StatBox stat='Wisdom' val={10} prof={0}/>
      <StatBox stat='Constitution' val={10} prof={0}/>
      <StatBox stat='Charisma' val={10} prof={0}/>

      <Button title='Edit' onPress={editStat}/>
    </View>
  );
}

/**************************************************
 * Component:   StatBox
 * 
 * Description: Individual Statbox component
 * 
 * Props:       stat  - Stat name
 *              val   - Stat numerical value
 *              prof  - Proficiency bonus
 **************************************************/
function StatBox(props) {
  return (
    <View style={styles.statBox}>
      <Text style={styles.statTitle}>{props.stat + ': ' + props.val}</Text>
      <View style={flexDirection='row'}>
        <Text style={styles.statText}>{'Modifier: ' + ((props.val - 10) / 2)}</Text>
        <Text style={styles.statText}>{'Saving Throw: ' + (((props.val - 10) / 2) + props.prof)}</Text>
      </View>
    </View>
  );
}

/**************************************************
 * Component:   AbilityDrop
 * 
 * Description: Drop down component for character
 *              abilities
 * 
 * Props:       none
 **************************************************/
function AbilityDrop(props) {
  const [abilites, setAbilites] = useState(null);
  const [visible, setVisible] = useState(false);
  
  const [formTitle, setTitle] = useState('');
  const [formDesc, setDesc] = useState('');
  const [formUses, setUses] = useState(null);


  useEffect(() => {
    db.transaction((tx) => {
      tx.executeSql(
        `select * from abilities;`,
        [],
        (_, { rows: { _array } }) => setAbilites(_array)
      );
    });
  }, []);

  const toggleForm = () => {
    setVisible(!visible);
  }

  const submitForm = () => {
    if(addAbility(formTitle, formDesc, formUses)) {
      toggleForm();
    }
  }

  const addAbility = (title, desc, uses) => {
    if (
      title === null || title == '' ||
      desc === null || desc == '' ||
      uses === null
    ) {
      return false;
    }
  
    db.transaction(
      (tx) => {
        tx.executeSql(`insert into abilities (title, desc, uses) values (?, ?, ?)`, [title, desc, uses]);
        tx.executeSql(`select * from abilities`, [], (_, { rows }) => console.log(JSON.stringify(rows)));
      },
      null,
      // https://stackoverflow.com/questions/50058827/react-how-to-rerender-component-after-post-data-to-database-mongodb
      () => {this.setState(prevState=>({abilites: [[title, desc, uses], ...prevState.abilites]}))}
    );
  
    return true;
  };


  return(
    <View>
      {!!abilites && (
        abilites.map(({id, title, desc, uses}) => (
            <Dropdown style={styles.dropdown} key={id} title={title} data={desc} dropSub={'Uses: ' + uses}/>
      )))}

      <Overlay isVisible={visible} onBackdropPress={toggleForm}>
        <View style={styles.abilForm}>
          <Input placeholder='Abiltiy Title' onChangeText={(text) => { setTitle(text) }}/>
          <Input placeholder='Description' onChangeText={(text) => { setDesc(text) }}/>
          <Input placeholder='Uses' onChangeText={(text) => { setUses(text) }} keyboardType='number-pad'/>
          <Button title='Add Ability' onPress={submitForm}/>
        </View>
      </Overlay>

      <Button title='Add' onPress={toggleForm}/>
    </View>
  );
}

/**************************************************
 * Component: Dropdown
 * 
 * Desc:      Base dropdown component
 * 
 * Props:     title   - Dropdown title 
 *            data    - main data inside dropdown
 *            dropSub - Sub title (right aligned)
 **************************************************/
function Dropdown(props) {
  const [open, setopen] = useState(false);
  const [edit, setEdit] = useState(false);
  const [data, setData] = useState(props.data);

  const onToggle = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setopen(!open);
    setEdit(false);
  };

  const onSave = () => {
    // Todo: Query stuff

    setEdit(false);
  }

  return (
    <TouchableOpacity style={[styles.dropdown, !open && { height: 64 }]} onPress={onToggle} activeOpacity={1}>
      <View style={styles.dropCloseCont}>
        <Text style={styles.dropTitle}>{props.title}</Text>
        <Text style={styles.dropSub}>{props.dropSub}</Text>
      </View>
      {open && (
        <View>
          {!edit && (
            <View>
              <Text style={styles.dropText}>{data}</Text>
              <Button title='Edit' onPress={() => { setEdit(true) }}/>
            </View>
          )}
          
          {edit && (
            <View>
              <TextInput 
                style={styles.dropEdit} 
                editable value={data} 
                onChangeText={(text) => setData(text)}
                multiline
              />
              <Button title='Save' onPress={() => {onSave}}/>
            </View>
          )}
        </View>
      )}
    </TouchableOpacity>
  );
}


const styles = StyleSheet.create({
  statPage: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
    alignContent: 'space-around',
    paddingVertical: 30,
  },

  statBox: {
    borderColor: '#000',
    borderWidth: 1,
    alignItems: 'center',
    width: 150,
    marginVertical: 20,
    paddingVertical: 5,
  },

  statTitle: {
    fontSize: 20,
  },

  statText: {
    fontSize: 16,
  },

  abilPage: {
    alignItems: 'center',
    paddingVertical: 50,
  },
  
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 50,
  },

  dropdown: {
    width: 350,
    borderWidth: 1,
    paddingHorizontal: 20,
    overflow: 'hidden',
    paddingVertical: 10,
    marginBottom: 5,
  },

  dropCloseCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  dropTitle: {
    fontSize: 32,
  },

  dropSub: {
    fontSize: 20,
    paddingVertical: 6,
  },

  dropText: {
    paddingVertical: 10,
  },

  dropEdit: {
    paddingVertical: 10,
  },

  abilForm: {
    width: 300,
  }
});
